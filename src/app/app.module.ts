import { NgModule } from '@angular/core';
import { ReactiveFormsModule } from '@angular/forms';
import { BrowserModule } from '@angular/platform-browser';
import { NgbModule } from '@ng-bootstrap/ng-bootstrap';

import { AppRoutingModule } from './app-routing.module';
import { AppComponent } from './app.component';
import { BackupComponent } from './backup/backup.component';
import { AddressComponent } from './help/address.component';
import { HelpComponent } from './help/help.component';
import { IconsModule } from './icons/icons.module';
import { LandingComponent } from './landing/landing.component';
import { PortfolioComponent } from './portfolio/portfolio.component';
import { TransactionsComponent } from './transactions/transactions.component';
import { AmountPipe } from './utils/amount.pipe';
import { AutofocusDirective } from './utils/autofocus.directive';
import { CurrencyPipe } from './utils/currency.pipe';
import { EmptyInputDirective } from './utils/empty.input.directive';
import { VarDirective } from './utils/ng-var.directive';
import { RoiPipe } from './utils/roi.pipe';
import { SafeUrlPipe } from './utils/safe-url.pipe';
import { SignumPipe } from './utils/signum.pipe';

@NgModule({
  declarations: [
    AppComponent,
    LandingComponent,
    PortfolioComponent,
    TransactionsComponent,
    BackupComponent,
    HelpComponent,
    AddressComponent,
    SignumPipe,
    RoiPipe,
    AmountPipe,
    CurrencyPipe,
    SafeUrlPipe,
    VarDirective,
    AutofocusDirective,
    EmptyInputDirective,
  ],
  imports: [
    BrowserModule,
    AppRoutingModule,
    NgbModule,
    ReactiveFormsModule,
    IconsModule,
  ],
  providers: [],
  bootstrap: [AppComponent],
})
export class AppModule { }
