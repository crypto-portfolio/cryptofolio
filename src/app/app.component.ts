import { Component, OnInit } from '@angular/core';

import { TransactionsService } from './services/transactions.service';

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.scss'],
})
export class AppComponent implements OnInit  {
  public title = 'portfolio';
  public isMenuCollapsed = true;
  public currencySet = true;

  constructor(
    private transactionsService: TransactionsService,
  ) {}

  public ngOnInit(): void {
    this.transactionsService.currency$.subscribe(currency => {
      this.currencySet = !!currency;
    });
  }
}
