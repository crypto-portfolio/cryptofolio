import { Component, OnInit } from '@angular/core';
import { FormBuilder, Validators } from '@angular/forms';
import { combineLatest, Observable, of } from 'rxjs';
import { filter, map, switchMap } from 'rxjs/operators';

import { IAssetTotal, PortfolioService } from '../services/portfolio.service';
import { ITokenImages, TokenService } from '../services/token.service';
import { TransactionsService } from '../services/transactions.service';
import { tokenValidator } from '../utils/token-validator';
import { findMatches } from '../utils/utils';

@Component({
  selector: 'app-portfolio',
  templateUrl: './portfolio.component.html',
  styleUrls: ['./portfolio.component.scss'],
})
export class PortfolioComponent implements OnInit {

  public assetTotals$: Observable<IAssetTotal[]>;
  public summary$: Observable<IAssetTotal>;
  public currency$: Observable<string | undefined>;
  public tokenImages$: Observable<ITokenImages>;

  public newTransactionActive: boolean = false;
  public newTransactionForm = this.fb.group({
    type: [null, [Validators.required]],
    asset: [null, [Validators.required]],
    date: [null, [Validators.required]],
    amount: [null, [Validators.required, Validators.min(0)]],
    costUSD: [null, [Validators.required, Validators.min(0)]],
    costNative: [null, [Validators.required, Validators.min(0)]],
    unitCostUSD: [null],
    unitCostNative: [null],
  });
  public searchToken: (text$: Observable<string>) => Observable<string[]>;

  constructor(
    private fb: FormBuilder,
    private portfolioService: PortfolioService,
    private transactionsService: TransactionsService,
    private tokenService: TokenService,
  ) {
    this.assetTotals$ = this.portfolioService.assetTotals$;
    this.summary$ = this.portfolioService.summary$;
    this.currency$ = this.transactionsService.currency$;
    this.tokenImages$ = this.tokenService.tokenImages$;
    this.searchToken = (text$: Observable<string>): Observable<string[]> => combineLatest([this.tokenService.tokenNames$, text$]).pipe(
      map(([tokenNames, term]) => findMatches(tokenNames, term)),
    );
  }

  public ngOnInit(): void {
    this.tokenService.tokenNames$.subscribe(tokenNames => {
      const asset = this.newTransactionForm.get('asset')!;
      asset.setValidators([tokenValidator(tokenNames)]);
      asset.updateValueAndValidity();
    });

    combineLatest([
      this.newTransactionForm.get('asset')!.statusChanges,
      this.newTransactionForm.get('asset')!.valueChanges,
      this.newTransactionForm.get('date')!.valueChanges,
    ]).pipe(
      switchMap(([assetStatus, asset, date]) =>
        assetStatus === 'VALID' && asset !== null && date ? this.tokenService.tokenPrice(asset, new Date(date)) : of(null),
      ),
    ).subscribe(cost => {
      this.newTransactionForm.patchValue({ unitCostUSD: cost?.usd, unitCostNative: cost?.native });
    });

    combineLatest([
      this.newTransactionForm.get('unitCostUSD')!.valueChanges,
      this.newTransactionForm.get('unitCostNative')!.valueChanges,
      this.newTransactionForm.get('amount')!.valueChanges,
    ]).pipe(
      filter(([, , amount]) => amount !== null),
      map(([unitCostUSD, unitCostNative, amount]) => ({
        usd: unitCostUSD && (unitCostUSD * amount).toFixed(2),
        native: unitCostNative && (unitCostNative * amount).toFixed(2),
      })),
    ).subscribe(cost => {
      this.newTransactionForm.patchValue({ costUSD: cost?.usd, costNative: cost?.native });
    });
  }

  public newPurchase(): void {
    this.newTransactionActive = true;
    this.newTransactionForm.reset();
    this.newTransactionForm.patchValue({ type: 'buy', date: new Date() });
  }

  public newSale(): void {
    this.newTransactionActive = true;
    this.newTransactionForm.reset();
    this.newTransactionForm.patchValue({ type: 'sell', date: new Date() });
  }

  public submitAdd(): void {
    const transaction = {
      id: '',
      type: this.newTransactionForm.get('type')!.value,
      asset: this.newTransactionForm.get('asset')!.value.toUpperCase(),
      date: this.newTransactionForm.get('date')!.value,
      amount: this.newTransactionForm.get('amount')!.value,
      costUSD: this.newTransactionForm.get('costUSD')!.value,
      costNative: this.newTransactionForm.get('costNative')!.value,
    };
    this.transactionsService.newTransaction(transaction).subscribe(() => {
      this.newTransactionActive = false;
    });
  }

  public cancelAdd(): void {
    this.newTransactionActive = false;
  }

  public newTransactionKey(event: KeyboardEvent): void {
    if (event.key === 'Enter' && this.newTransactionForm.valid) {
      this.submitAdd();
    } else if (event.key === 'Escape') {
      this.cancelAdd();
    }
  }

}
