import { Injectable } from '@angular/core';
import { combineLatest, Observable, of } from 'rxjs';
import { first, map } from 'rxjs/operators';

import { TransactionsService } from './transactions.service';

@Injectable({
  providedIn: 'root',
})
export class BackupService {

  constructor(
    private transactionsService: TransactionsService,
  ) {}

  public export$: Observable<any> = combineLatest([
    this.transactionsService.currency$.pipe(first()),
    this.transactionsService.exportTransactions(),
  ]).pipe(
    map(([currency, transactions]) => ({ currency, transactions })),
  );

  public import({ currency, transactions }: any): Observable<void> {
    return combineLatest([
      of(this.transactionsService.setCurrency$.next(currency)),
      this.transactionsService.importTransactions(transactions),
    ]).pipe(
      map(() => {}),
    );
  }
}
