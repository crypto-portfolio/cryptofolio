import { Injectable } from '@angular/core';
import { groupBy, sum, toPairs } from 'lodash';
import { combineLatest, Observable } from 'rxjs';
import { map } from 'rxjs/operators';

import { ITokenPrices, TokenService } from './token.service';
import { ITransaction, TransactionsService } from './transactions.service';

export interface IAssetTotalBase {
  asset: string;
  amount: number;
  costUSD: number;
  costNative: number;
}
export interface IAssetTotal extends IAssetTotalBase {
  unitCostUSD?: number;
  unitCostNative?: number;
  valueUSD?: number;
  valueNative?: number;
  roiUSD?: number;
  roiNative?: number;
}

@Injectable({
  providedIn: 'root',
})
export class PortfolioService {

  public assetTotals$: Observable<IAssetTotal[]>;
  public summary$: Observable<IAssetTotal>;

  constructor(
    private transactionsService: TransactionsService,
    private tokenService: TokenService,
  ) {
    this.assetTotals$ = combineLatest([this.transactionsService.transactions$, this.tokenService.tokenPrices$]).pipe(
      map(([totals, tokenPrices]) => this.addPrice(this.summarize(totals), tokenPrices)),
    );
    this.summary$ = this.assetTotals$.pipe(map(this.totalize));
  }

  private summarize(transactions: ITransaction[]): IAssetTotalBase[] {
    return toPairs(groupBy(transactions, transaction => transaction.asset)).map(([asset, group]) => ({
      asset,
      amount: sum(group.map(transaction => transaction.amount * (transaction.type === 'buy' ? 1 : -1))),
      costUSD: sum(group.map(transaction => transaction.costUSD * (transaction.type === 'buy' ? 1 : -1))),
      costNative: sum(group.map(transaction => transaction.costNative * (transaction.type === 'buy' ? 1 : -1))),
    }));
  }

  private addPrice(totals: IAssetTotalBase[], tokenPrices: ITokenPrices): IAssetTotal[] {
    return totals.map(total => {
      const valueUSD = tokenPrices[total.asset] ? tokenPrices[total.asset].usd * total.amount : undefined;
      const valueNative = tokenPrices[total.asset] ? tokenPrices[total.asset].native * total.amount : undefined;
      return {
        ...total,
        valueUSD,
        valueNative,
        unitCostUSD: total.amount ? total.costUSD / total.amount : 0,
        unitCostNative: total.amount ? total.costNative / total.amount : 0,
        roiUSD: valueUSD && (valueUSD - total.costUSD) / total.costUSD,
        roiNative: valueNative && (valueNative - total.costNative) / total.costNative,
      };
    });
  }

  private totalize(totals: IAssetTotal[]): IAssetTotal {
    const result = totals.reduce((total, item) => ({
      ...total,
      costUSD: total.costUSD + item.costUSD,
      costNative: total.costNative + item.costNative,
      valueUSD: item.valueUSD !== undefined && total.valueUSD !== undefined ? item.valueUSD + total.valueUSD : undefined,
      valueNative: item.valueNative !== undefined && total.valueNative !== undefined ? item.valueNative + total.valueNative : undefined,
    }), { asset: 'Total', amount: 0, costUSD: 0, costNative: 0, valueUSD: 0, valueNative: 0, roiUSD: undefined, roiNative: undefined });
    return {
      ...result,
      roiUSD: result.valueUSD && (result.valueUSD - result.costUSD) / result.costUSD,
      roiNative: result.valueNative && (result.valueNative - result.costNative) / result.costNative,
    };
  }

}
