import { Injectable } from '@angular/core';
import { StorageMap } from '@ngx-pwa/local-storage';
import { sortBy } from 'lodash';
import { concat, Observable, of, Subject } from 'rxjs';
import { map, switchMap, tap } from 'rxjs/operators';
import { v1 as uuidv1 } from 'uuid';

export interface ITransactionBase {
  id: string;
  type: 'buy' | 'sell';
  asset: string;
  date: Date;
  amount: number;
  costUSD: number;
  costNative: number;
}
export interface ITransaction extends ITransactionBase {
  unitCostUSD: number;
  unitCostNative: number;
  total: number;
  totalUSD: number;
  totalNative: number;
}

@Injectable({
  providedIn: 'root',
})
export class TransactionsService {

  private transactions: ITransactionBase[] = [];

  constructor(private storage: StorageMap) {
    this.setCurrency$.subscribe(currency => {
      this.storage.set('currency', currency).subscribe(() => {});
    });
  }

  private refresh$: Subject<void> = new Subject();

  public transactions$: Observable<ITransaction[]> =
    concat(
      this.storage.get('transactions').pipe(
        tap(v => {
          this.transactions = v as any || this.transactions;
        }),
        map(() => undefined),
      ),
      this.refresh$.pipe(
        switchMap(() =>
          this.storage.set('transactions', this.transactions),
        ),
      ),
    ).pipe(
      map(() => this.postprocess(this.transactions)),
    );

  public setCurrency$: Subject<string | undefined> = new Subject();
  public currency$: Observable<string | undefined> = concat(
    this.storage.get('currency', { type: 'string' }),
    this.setCurrency$,
  );

  public newTransaction(transaction: ITransactionBase): Observable<ITransactionBase> {
    this.transactions.push({ ...transaction, id: uuidv1() });
    this.refresh$.next();
    return of(transaction);
  }

  public deleteTransaction(id: string): Observable<void> {
    this.transactions = this.transactions.filter(transaction => transaction.id !== id);
    this.refresh$.next();
    return of(undefined);
  }

  public exportTransactions(): Observable<any> {
    return this.storage.get('transactions');
  }

  public importTransactions(transactions: any): Observable<void> {
    this.transactions = transactions;
    this.refresh$.next();
    return this.storage.set('transactions', transactions);
  }

  private postprocess(transactions: ITransactionBase[]): ITransaction[] {
    const total: { [asset: string ]: number } = {};
    const totalUSD: { [asset: string ]: number } = {};
    const totalNative: { [asset: string ]: number } = {};
    return sortBy(transactions, transaction => transaction.date).map(transaction => {
      total[transaction.asset] = (total[transaction.asset] || 0) + transaction.amount * (transaction.type === 'buy' ? 1 : -1);
      totalUSD[transaction.asset] = (totalUSD[transaction.asset] || 0) + transaction.costUSD * (transaction.type === 'buy' ? 1 : -1);
      totalNative[transaction.asset] = (totalNative[transaction.asset] || 0) +
        transaction.costNative * (transaction.type === 'buy' ? 1 : -1);
      return {
        ...transaction,
        unitCostUSD: transaction.amount ? transaction.costUSD / transaction.amount : 0,
        unitCostNative: transaction.amount ? transaction.costNative / transaction.amount : 0,
        total: total[transaction.asset],
        totalUSD: totalUSD[transaction.asset],
        totalNative: totalNative[transaction.asset],
      };
    });
  }
}
