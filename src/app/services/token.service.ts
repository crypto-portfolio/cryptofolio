import { Injectable } from '@angular/core';
import { fromPairs, isEqual, mapKeys, mapValues, uniq } from 'lodash';
import { combineLatest, forkJoin, interval, Observable } from 'rxjs';
import { ajax } from 'rxjs/ajax';
import { distinctUntilChanged, filter, map, shareReplay, startWith, switchMap } from 'rxjs/operators';

import { TransactionsService } from './transactions.service';

interface IToken {
  id: string;
  symbol: string;
  name: string;
}
type ITokenField = 'id' | 'symbol' | 'name';
interface ITokenIndex {
  [key: string]: string;
}
export interface IPrice {
  usd: number;
  native: number;
}
export interface ITokenPrices {
  [key: string]: IPrice;
}
export interface ITokenImages {
  [key: string]: string;
}

const tokenIndexFactory = (tokens$: Observable<IToken[]>, key: ITokenField, value: ITokenField): Observable<ITokenIndex> =>
  tokens$.pipe(
    map(tokens => fromPairs(tokens.reverse().map(token => [token[key], token[value]]))),
  );

const API_ROOT = 'https://api.coingecko.com/api/v3';

@Injectable({
  providedIn: 'root',
})
export class TokenService {

  constructor(private transactionsService: TransactionsService) {}

  private tokens$: Observable<IToken[]> = ajax(`${API_ROOT}/coins/list`).pipe(
    map(({ response }) => response),
    shareReplay(1),
  );
  private tokenSymbolToId$: Observable<ITokenIndex> = tokenIndexFactory(this.tokens$, 'symbol', 'id');
  private tokenIdToSymbol$: Observable<ITokenIndex> = tokenIndexFactory(this.tokens$, 'id', 'symbol');
  public tokenNames$: Observable<string[]> = this.tokens$.pipe(map(tokens => tokens.map(({ symbol }) => symbol.toUpperCase())));

  public userTokenNames$: Observable<string[]> = this.transactionsService.transactions$.pipe(
    map(transactions => uniq(transactions.map(({ asset }) => asset))),
    distinctUntilChanged(isEqual),
  );
  public tokenPrices$: Observable<ITokenPrices> =
    combineLatest([this.transactionsService.currency$, this.userTokenNames$, this.tokenSymbolToId$, this.tokenIdToSymbol$]).pipe(
      filter(([currency]) => !!currency),
      switchMap(([currency, tokens, ids, revIds]) =>
        interval(5 * 60 * 1000).pipe(
          startWith(-1),
          switchMap(() => {
            const tokenIds = tokens.map(token => ids[token.toLowerCase()]).join(',');
            return ajax(`${API_ROOT}/simple/price?ids=${tokenIds}&vs_currencies=usd,${currency!.toLowerCase()}`).pipe(
              map(({ response }) => mapValues(mapKeys(response, (_, key) => revIds[key].toUpperCase()), result => ({
                usd: result.usd,
                native: result[currency!.toLowerCase()],
              }))),
            );
          }),
        ),
      ),
      shareReplay(1),
    );

  public tokenImages$: Observable<ITokenImages> =
    combineLatest([this.userTokenNames$, this.tokenSymbolToId$]).pipe(
      switchMap(([tokens, ids]) => forkJoin(fromPairs(tokens.map(token => [token,
        ajax(`${API_ROOT}/coins/${ids[token.toLowerCase()]}?localization=false&tickers=false&market_data=false&community_data=false&developer_data=false&sparkline=false`).pipe(
          map(({ response: { image } }) => image.thumb || image.small),
        ),
      ])))),
      shareReplay(1),
    );

  public currencies$: Observable<string[]> = ajax(`${API_ROOT}/simple/supported_vs_currencies`).pipe(
    map(({ response }) => response),
  );

  private formatDate(date: Date): string {
    return `${date.getDate()}-${date.getMonth() + 1}-${date.getFullYear()}`;
  }

  public tokenPrice(token: string, date: Date): Observable<IPrice> {
    return combineLatest([this.transactionsService.currency$, this.tokenSymbolToId$]).pipe(
      filter(([currency]) => !!currency),
      switchMap(([currency, ids]) =>
        ajax(`${API_ROOT}/coins/${ids[token.toLowerCase()]}/history?date=${this.formatDate(date)}`).pipe(
          map(({ response: { market_data } }) => ({
            usd: market_data?.current_price?.usd,
            native: market_data?.current_price && market_data?.current_price[currency!.toLowerCase()],
          })),
        ),
      ),
    );
  }

}
