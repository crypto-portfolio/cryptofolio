import { Component, OnInit } from '@angular/core';
import { FormBuilder } from '@angular/forms';

import { TokenService } from '../services/token.service';
import { TransactionsService } from '../services/transactions.service';

@Component({
  selector: 'app-landing',
  templateUrl: './landing.component.html',
  styleUrls: ['./landing.component.scss'],
})
export class LandingComponent implements OnInit {

  public currencyForm = this.fb.group({
    currency: [''],
  });
  public currencies: string[] = [];

  constructor(
    private fb: FormBuilder,
    private tokenService: TokenService,
    private transactionsService: TransactionsService,
  ) { }

  public ngOnInit(): void {
    this.tokenService.currencies$.subscribe(currencies => {
      this.currencies = currencies.map(currency => currency.toUpperCase());
    });
    this.transactionsService.currency$.subscribe(currency => {
      this.currencyForm.get('currency')!.setValue(currency || '', { emitEvent: false });
    });
    this.currencyForm.get('currency')!.valueChanges.subscribe(currency => {
      this.transactionsService.setCurrency$.next(currency || undefined);
    });
    this.transactionsService.transactions$.subscribe(transactions => {
      if (transactions.length > 0) {
        this.currencyForm.get('currency')!.disable();
      } else {
        this.currencyForm.get('currency')!.enable();
      }
    });
  }

}
