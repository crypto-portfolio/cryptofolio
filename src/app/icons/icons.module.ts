import { NgModule } from '@angular/core';

import { FeatherModule } from 'angular-feather';
import { Check, Clipboard, Delete, Download, Filter, MinusSquare, PlusSquare, Search, Slash, Trash2, Upload, X } from 'angular-feather/icons';

const icons = {
  Check, Clipboard, Delete, Download, Filter, MinusSquare, PlusSquare, Search, Slash, Trash2, Upload, X,
};

@NgModule({
  imports: [
    FeatherModule.pick(icons),
  ],
  exports: [
    FeatherModule,
  ],
})
export class IconsModule { }
