import { Component, OnInit } from '@angular/core';
import { FormBuilder } from '@angular/forms';
import { Router } from '@angular/router';
import * as FileSaver from 'file-saver';
import { of } from 'rxjs';
import { catchError, map, switchMap, tap } from 'rxjs/operators';

import { BackupService } from '../services/backup.service';

@Component({
  selector: 'app-backup',
  templateUrl: './backup.component.html',
  styleUrls: ['./backup.component.scss'],
})
export class BackupComponent implements OnInit {

  public importForm = this.fb.group({
    file: [null],
  });
  public errorMessage?: string;

  constructor(
    private backupService: BackupService,
    private fb: FormBuilder,
    private router: Router,
  ) { }

  public ngOnInit(): void {
  }

  public import(event: Event): void {
    const file = ((event.target! as any).file as HTMLInputElement).files![0];
    this.errorMessage = undefined;
    const reader = new FileReader();
    reader.onload = e => this.doImport(e.target!.result as string);
    reader.readAsText(file);
  }

  public doImport(contents: string): void {
    of(contents).pipe(
      map(contentsString => JSON.parse(contentsString)),
      switchMap(contentsObject => this.backupService.import(contentsObject)),
      tap(() => this.router.navigate(['/portfolio'])),
      catchError(error => this.errorMessage = error.message || 'unknown error'),
    ).subscribe(() => {});
  }

  public export(): void {
    this.backupService.export$.subscribe(data => {
      const blob = new Blob([JSON.stringify(data)], { type: 'application/json' });
      FileSaver.saveAs(blob, 'export.json');
    });
  }

  public clear(): void {
    this.backupService.import({
      transactions: [],
    }).subscribe(() => {
      this.router.navigate(['/']);
    });
  }

}
