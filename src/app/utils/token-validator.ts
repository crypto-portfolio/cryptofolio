import { AbstractControl, ValidatorFn } from '@angular/forms';

export function tokenValidator(tokens: string[]): ValidatorFn {
  return (control: AbstractControl): {[key: string]: any} | null => {
    return tokens.includes(control.value?.toUpperCase()) ? null : { token: true };
  };
}
