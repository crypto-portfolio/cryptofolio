import { Pipe, PipeTransform } from '@angular/core';

@Pipe({ name: 'signum' })
export class SignumPipe implements PipeTransform {
  public transform(value: number): string {
    return value > 0 ? '+' : value < 0 ? '-' : '';
  }
}
