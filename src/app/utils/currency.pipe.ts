import { DecimalPipe } from '@angular/common';
import { Pipe, PipeTransform } from '@angular/core';

@Pipe({ name: 'currency' })
export class CurrencyPipe extends DecimalPipe implements PipeTransform {
  public transform(value: number): string | null {
    return super.transform(value, '1.2-2');
  }
}
