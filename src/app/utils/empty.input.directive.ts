import { Directive, ElementRef, OnDestroy, OnInit } from '@angular/core';

@Directive({
  selector: '[appEmptyInput]',
})
export class EmptyInputDirective implements OnInit, OnDestroy {

  constructor(private el: ElementRef) {}

  public ngOnInit(): void {
    this.el.nativeElement.addEventListener('change', this.update);
    this.update();
  }

  public ngOnDestroy(): void {
    this.el.nativeElement.removeEventListener('change', this.update);
  }

  public update = (): void => {
    if (this.el.nativeElement.value === '') {
      this.el.nativeElement.classList.add('empty');
    } else {
      this.el.nativeElement.classList.remove('empty');
    }
  }

}
