import { flatten, uniq } from 'lodash';

export function findMatches(array: string[], term: string): string[] {
  if (term.length < 2) {
    return [];
  }
  const r1 = new RegExp('^' + term + '$', 'i');
  const r2 = new RegExp('^' + term, 'i');
  const r3 = new RegExp(term, 'i');
  return uniq(flatten([
    array.filter(r1.test.bind(r1)),
    array.filter(r2.test.bind(r2)),
    array.filter(r3.test.bind(r3)),
  ])).slice(0, 5);
}

export function findPrefixMatches(array: string[], term: string): string[] {
  if (term.length < 1) {
    return [];
  }
  const r = new RegExp('^' + term, 'i');
  return uniq(flatten([
    array.filter(r.test.bind(r)),
  ])).slice(0, 5);
}
