import { DecimalPipe } from '@angular/common';
import { Pipe, PipeTransform } from '@angular/core';

@Pipe({ name: 'amount' })
export class AmountPipe extends DecimalPipe implements PipeTransform {
  public transform(value: number): string | null {
    return super.transform(value, '1.4-4');
  }
}
