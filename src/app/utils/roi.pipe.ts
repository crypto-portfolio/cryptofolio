import { PercentPipe } from '@angular/common';
import { Pipe, PipeTransform } from '@angular/core';

@Pipe({ name: 'roi' })
export class RoiPipe extends PercentPipe implements PipeTransform {
  public transform(value: number): string | null {
    return super.transform(value, '1.2');
  }
}
