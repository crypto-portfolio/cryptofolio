import { Directive, Input, TemplateRef, ViewContainerRef } from '@angular/core';

@Directive({
  selector: '[appNgVar]',
})
export class VarDirective {
  @Input()
  set appNgVar(context: any) {
    this.context.$implicit = this.context.appNgVar = context;
    this.updateView();
  }

  private context: any = {};

  constructor(private vcRef: ViewContainerRef, private templateRef: TemplateRef<any>) {}

  private updateView() {
    this.vcRef.clear();
    this.vcRef.createEmbeddedView(this.templateRef, this.context);
  }
}
