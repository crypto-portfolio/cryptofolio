import { Component, ElementRef, Input, OnInit, ViewChild } from '@angular/core';

@Component({
  selector: 'app-address',
  templateUrl: './address.component.html',
})
export class AddressComponent implements OnInit {

  @Input()
  public address!: string;

  @ViewChild('field')
  public field!: ElementRef<HTMLInputElement>;

  constructor() { }

  public ngOnInit(): void {
  }

  public copy(): void {
    this.field.nativeElement.select();
    document.execCommand('copy');
  }
}
