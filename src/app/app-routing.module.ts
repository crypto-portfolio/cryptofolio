import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';

import { BackupComponent } from './backup/backup.component';
import { HelpComponent } from './help/help.component';
import { LandingComponent } from './landing/landing.component';
import { PortfolioComponent } from './portfolio/portfolio.component';
import { TransactionsComponent } from './transactions/transactions.component';

const routes: Routes = [
  { path: 'portfolio', component: PortfolioComponent },
  { path: 'transactions', component: TransactionsComponent },
  { path: 'backup', component: BackupComponent },
  { path: 'help', component: HelpComponent },
  { path: '**', component: LandingComponent },
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule],
})
export class AppRoutingModule { }
